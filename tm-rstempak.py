#fst-rstempak.py
import sys

class machine:
    def __init__(self, description):
        self.name = description[0].strip().split(",")[0]
        self.k_tapes = description[0].strip().split(",")[1]
        self.tape_length = description[0].strip().split(",")[2]
        self.maxs_steps = description[0].strip().split(",")[3]

        self.input_alphabet = description[1].strip().split(",")
        self.states = description[2].strip().split(",")
        self.start_state = description[3].strip()
        self.accepting_state = description[4].strip().split(",")[0]
        self.rejecting_state = description[4].strip().split(",")[1]
        self.output_alphabet = description[5].strip().split(",")
        self.transition_function = {}

        # echo name to stdout
        print self.name

        # parse lines for transition_function (format = transition_function[state][transition] = (rule_number, reachable_state, output char)
        for state in self.states:
            self.transition_function[state] = {}

        rule_number = 0
        for line in description[6:]:
            InitialStateName, InputSymbol, NewStateName, OutputSymbol, Direction = line.strip().split(",")

            # fill transition_function
            self.transition_function[InitialStateName][InputSymbol] = (rule_number, NewStateName, OutputSymbol, Direction)
            rule_number+=1

    def run_tests(self, tape_strings):
        ### init final data variables ###
        final_strings = []

        # dict for easy tape index math
        update_index = {'S': 0, 'R': 1, 'L': -1 }

        for i, line in enumerate(tape_strings):
            string = line.strip()
            print ("Tape {}: ".format(i+1) + string)
            output_string = list(string)

            ### test string ###
            InitialStateName = self.start_state
            tape_index = 0
            Stepnumber = 0

            while Stepnumber < self.maxs_steps and tape_index < self.tape_length and tape_index < len(string):
                InputSymbol = output_string[tape_index]

                #see if transition exists for current state
                try:
                    RuleNumber, NewStateName, OutputSymbol, Direction = self.transition_function[InitialStateName][InputSymbol]
                except:
                    break

                print "{},{},{},{},{},{},{},{}".format(Stepnumber,RuleNumber,tape_index,InitialStateName,InputSymbol,NewStateName,OutputSymbol,Direction)

                #update tape
                output_string[tape_index] = OutputSymbol

                #update indices
                tape_index+=update_index[Direction]
                Stepnumber+=1
                InitialStateName = NewStateName

            ### results ###
            if InitialStateName == self.accepting_state:
                print ("Accepted\n")
            else:
                print ("Rejected\n")
            final_strings.append("Tape {}: ".format(i+1) + "".join(output_string))

        return final_strings

def main():
    args = sys.argv[1:]
    if len(sys.argv) != 3:
        print ("Usage: python oneD-define-rstempak.py <machine file> <initial tape file>")
        return

    ### read machine description and tape ###
    machine_file = args[0]
    tape_file = args[1]

    with open(machine_file) as machinef:
        machine_description = machinef.readlines()

    with open(tape_file) as tapef:
        tape_strings = tapef.readlines()

    ### run tests ###
    my_machine = machine(machine_description)
    final_tapes = my_machine.run_tests(tape_strings)

    print ("Output Strings:")
    for string in final_tapes:
        print (string)

if __name__ == '__main__':
    main()
